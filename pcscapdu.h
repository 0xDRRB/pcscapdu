#pragma once

#define PCSCAPDUVERSION "PCSCapdu v0.1 - Copyright (c) 2022-2023 - Denis Bodor\nThis is free software with ABSOLUTELY NO WARRANTY.\n"

#define CONFFILE    ".pcscapdurc"
#define HISTFILE    ".pcscapdu_history"
#define LOGFILE     "./pcscapdu.log"

#define HISTSIZE             128
#define RAPDUMAXSZ           512
#define CAPDUMAXSZ           512
#define COLOR                  1
#define LOG                    0
#define S_SUCCESS         0x9000  // Command completed successfully
#define S_OK              0x9100  // OK (after additional data frame)
#define S_MORE            0x91af  // Additional data frame is expected to be sent

#define APDUMODE			   0
#define RAWMODE				   1

char **rl_commands_completion(const char *text, int start, int end);
char *rl_commands_generator(const char *text, int state);
int apfu_initconfig();
int strcardtransmit(SCARDHANDLE hCard, const char *line, uint8_t *rapdu, size_t *rapdulen, int quiet);
int cardtransmit(SCARDHANDLE hCard, uint8_t *capdu, size_t capdulen, uint8_t *rapdu, size_t *rapdulen, int quiet);
void failquit();
int listdevices();
void showaliases();
void cleanup();
