#pragma once

void apdu_help();
int isblankline(char *line);
void printhelp(char *binname);
size_t hex2array(const char *line, uint8_t *data, size_t len);

