-- Application Select for custom Javacard Applet
selapp = "00a4 0400 0a f276a288bcdeadbeef01"
getcount = { 0x80, 0x50, 0x00, 0x00, 0x01 }
get_hi = "8040 0000 0d"
getmeminfo = "8061 0000 04"


print("Select applet")
-- Send string APDU
response = sendstrapdu(selapp)
if (#response ~= 2) then
	print("Bad response size!")
	do return end
end
if (response[1] ~= 0x90 and response[2] ~= 0x00) then
	print("Bad response!")
	do return end
end
print("> OK")


print("Get counter value")
-- Send hex APDU
response2 = sendapdu(getcount)
-- get rid of status code
table.remove(response2, #response2)
table.remove(response2, #response2)
-- Display response
s = "> "
for key,value in ipairs(response2) do
	s = s .. string.format("%d", value)
end
print(s)


print("Get french 'Hello World'")
-- Send hex APDU
response3 = sendstrapdu(get_hi)
-- get rid of status code
text = { table.unpack(response3, 1, #response3 - 2) }
-- Display ASCII response
s = "> "
for key,value in ipairs(text) do
	s = s .. string.format("%c", value)
end
print(s)

print("Get flash/EEPROM size (for Javacard 2.2.2 max value is 32767 even if the card has more memory)")
-- Send hex APDU
response2 = sendstrapdu(getmeminfo)
-- get rid of status code
table.remove(response2, #response2)
table.remove(response2, #response2)
-- Display response
s = 0
for key,value in ipairs(response2) do
	s = s .. string.format("%02x", value)
end
print("> " .. tonumber(s,16) .. " bytes")
