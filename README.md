# PCSCapdu

A simple tool to exchange APDUs with a smartcard through PCSC Lite.

This utility is a fork of another project of mine ([NFCapdu](https://gitlab.com/0xDRRB/nfcapdu)), the same thing but for RFID/NFC tags with LibNFC. The two programs are maintained in parallel and differ simply by the middleware used (PC/SC vs LibNFC).

This program allows you to create an interactive session to send APDUs to a card and receive responses. I wrote this to easily experiment around JCOP Javacards and test APDUs before developing more specific code. The goal is to have a lighter and simpler tool than [GPShell](https://github.com/kaoh/globalplatform) or [GlobalPlatformPro](https://github.com/martinpaljak/GlobalPlatformPro), maybe this can be useful for someone else.

- It supports the creation of aliases (defined in `~/.pcscapdurc`) to simplify the sending of common commands
- APDU history is kept between sessions (in `~/.pcsapdu_history`)
- Have completion for commands and aliases
- You can enable or disable colors in config file (see `pcscapdurc_sample`)
- Support for Lua scripts with `-s FILE`. Take a look at `sample.lua` for more information
- Quit with `quit` or Ctrl+d (EOF)

Exemples:

- list readers:

```
$ ./pcscapdu -l
2 reader(s) found
0: IFD-NFC 00 00
1: SCM Microsystems Inc. SCR 335 [CCID Interface] (21120617208509) 00 00
```

- interactive session:

```
$ ./pcscapdu -d 1
2 reader(s) found
PCSCapdu v0.1 - Copyright (c) 2022-2023 - Denis Bodor
This is free software with ABSOLUTELY NO WARRANTY.
Reader: SCM Microsystems Inc. SCR 335 [CCID Interface] (21120617208509) 00 00 (length 70 bytes)
  Protocol: T1 (2)
  State:    0x34
  Prot:     2
  ATR (18): 3B F8 13 00 00 81 31 FE 45 4A 43 4F 50 76 32 34 31 B7
Logging enabled (/home/denis/pcscapdu.log openned)
8 alias loaded
Use 'help' to get a list of available commands.

APDU> alias
Defined aliases:
  selstapp = 00a4 0400 07 d2760000850101 00
  selstfile = 00a4 000c 02 e101
  readstfile = 00b0 0000 12
  selapp = 00a4 0400 0a f276a288bcdeadbeef01
  gethello = D040 0000
  deccounter = D052 0000
  inccounter = D051 0000
  getcounter = D050 0000

APDU> selapp
=> 00 a4 04 00 0a f2 76 a2 88 bc de ad be ef 01
<= 90 00 (2)

APDU> D040 0000
=> d0 40 00 00
<= 43 6f 75 63 6f 75 20 4d 6f 6e 64 65 21 90 00 (15)

APDU> getcounter
=> d0 50 00 00
<= 04 90 00 (3)

APDU> inccounter
=> d0 51 00 00
<= 90 00 (2)

APDU> d0 50 00 00
=> d0 50 00 00
<= 05 90 00 (3)

APDU> quit
Bye. Have a nice day
```

- use Lua script:

```
$ ./pcscapdu -d 1 -s sample.lua
2 reader(s) found
PCSCapdu v0.1 - Copyright (c) 2022-2023 - Denis Bodor
This is free software with ABSOLUTELY NO WARRANTY.
Reader: SCM Microsystems Inc. SCR 335 [CCID Interface] (21120617208509) 00 00 (length 70 bytes)
  Protocol: T1 (2)
  State:    0x34
  Prot:     2
  ATR (18): 3B F8 13 00 00 81 31 FE 45 4A 43 4F 50 76 32 34 31 B7
Logging enabled (/home/denis/pcscapdu.log openned)

> Select applet
OK
> Get counter value
5
> Get french 'Hello World'
Coucou Monde!

```
