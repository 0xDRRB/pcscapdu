#include <stdint.h>
#include <stdio.h>
#include "statusres.h"

// https://www.eftlab.com/knowledge-base/complete-list-of-apdu-responses/

void
getstrstatus(uint16_t code, char *str, size_t len) {
	switch(code) {
	case 0x6200: snprintf(str, len, "No information given (NV-Ram not changed)"); return;
	case 0x6201: snprintf(str, len, "NV-Ram not changed 1"); return;
	case 0x6281: snprintf(str, len, "Part of returned data may be corrupted"); return;
	case 0x6282: snprintf(str, len, "End of file/record reached before reading Le bytes"); return;
	case 0x6283: snprintf(str, len, "Selected file invalidated"); return;
	case 0x6284: snprintf(str, len, "Selected file is not valid. FCI not formated according to ISO"); return;
	case 0x6285: snprintf(str, len, "No input data available from a sensor on the card"); return;
	case 0x62A2: snprintf(str, len, "Wrong R-MAC"); return;
	case 0x62A4: snprintf(str, len, "Card locked (during reset())"); return;
	case 0x62F1: snprintf(str, len, "Wrong C-MAC"); return;
	case 0x62F3: snprintf(str, len, "Internal reset"); return;
	case 0x62F5: snprintf(str, len, "Default agent locked"); return;
	case 0x62F7: snprintf(str, len, "Cardholder locked"); return;
	case 0x62F8: snprintf(str, len, "Basement is current agent"); return;
	case 0x62F9: snprintf(str, len, "CALC Key Set not unblocked"); return;
	case 0x6300: snprintf(str, len, "No information given (NV-Ram changed)"); return;
	case 0x6381: snprintf(str, len, "File filled up by the last write. Loading/updating is not allowed"); return;
	case 0x6382: snprintf(str, len, "Card key not supported"); return;
	case 0x6383: snprintf(str, len, "Reader key not supported"); return;
	case 0x6384: snprintf(str, len, "Plaintext transmission not supported"); return;
	case 0x6385: snprintf(str, len, "Secured transmission not supported"); return;
	case 0x6386: snprintf(str, len, "Volatile memory is not available"); return;
	case 0x6387: snprintf(str, len, "Non-volatile memory is not available"); return;
	case 0x6388: snprintf(str, len, "Key number not valid"); return;
	case 0x6389: snprintf(str, len, "Key length is not correct"); return;
	case 0x63C0: snprintf(str, len, "Verify fail, no try left"); return;
	case 0x63C1: snprintf(str, len, "Verify fail, 1 try left"); return;
	case 0x63C2: snprintf(str, len, "Verify fail, 2 tries left"); return;
	case 0x63C3: snprintf(str, len, "Verify fail, 3 tries left"); return;
	case 0x63F1: snprintf(str, len, "More data expected"); return;
	case 0x63F2: snprintf(str, len, "More data expected and proactive command pending"); return;
	case 0x6400: snprintf(str, len, "No information given (NV-Ram not changed)"); return;
	case 0x6401: snprintf(str, len, "Command timeout. Immediate response required by the card"); return;
	case 0x6500: snprintf(str, len, "No information given"); return;
	case 0x6501: snprintf(str, len, "Write error. Memory failure. There have been problems in writing or reading the EEPROM"); return;
	case 0x6581: snprintf(str, len, "Memory failure"); return;
	case 0x6600: snprintf(str, len, "Error while receiving (timeout)"); return;
	case 0x6601: snprintf(str, len, "Error while receiving (character parity error)"); return;
	case 0x6602: snprintf(str, len, "Wrong checksum"); return;
	case 0x6603: snprintf(str, len, "The current DF file without FCI"); return;
	case 0x6604: snprintf(str, len, "No SF or KF under the current DF"); return;
	case 0x6669: snprintf(str, len, "Incorrect Encryption/Decryption Padding"); return;
	case 0x6700: snprintf(str, len, "Wrong length"); return;
	case 0x6800: snprintf(str, len, "No information given (The request function is not supported by the card)"); return;
	case 0x6881: snprintf(str, len, "Logical channel not supported"); return;
	case 0x6882: snprintf(str, len, "Secure messaging not supported"); return;
	case 0x6883: snprintf(str, len, "Last command of the chain expected"); return;
	case 0x6884: snprintf(str, len, "Command chaining not supported"); return;
	case 0x6900: snprintf(str, len, "No information given (Command not allowed)"); return;
	case 0x6901: snprintf(str, len, "Command not accepted (inactive state)"); return;
	case 0x6981: snprintf(str, len, "Command incompatible with file structure"); return;
	case 0x6982: snprintf(str, len, "Security condition not satisfied"); return;
	case 0x6983: snprintf(str, len, "Authentication method blocked"); return;
	case 0x6984: snprintf(str, len, "Referenced data reversibly blocked (invalidated)"); return;
	case 0x6985: snprintf(str, len, "Conditions of use not satisfied"); return;
	case 0x6986: snprintf(str, len, "Command not allowed (no current EF)"); return;
	case 0x6987: snprintf(str, len, "Expected secure messaging (SM) object missing"); return;
	case 0x6988: snprintf(str, len, "Incorrect secure messaging (SM) data object"); return;
	case 0x698D: snprintf(str, len, "Reserved"); return;
	case 0x6996: snprintf(str, len, "Data must be updated again"); return;
	case 0x69E1: snprintf(str, len, "POL1 of the currently Enabled Profile prevents this action"); return;
	case 0x69F0: snprintf(str, len, "Permission Denied"); return;
	case 0x69F1: snprintf(str, len, "Permission Denied – Missing Privilege"); return;
	case 0x6A00: snprintf(str, len, "No information given (Bytes P1 and/or P2 are incorrect)"); return;
	case 0x6A80: snprintf(str, len, "The parameters in the data field are incorrect"); return;
	case 0x6A81: snprintf(str, len, "Function not supported"); return;
	case 0x6A82: snprintf(str, len, "File not found"); return;
	case 0x6A83: snprintf(str, len, "Record not found"); return;
	case 0x6A84: snprintf(str, len, "There is insufficient memory space in record or file"); return;
	case 0x6A85: snprintf(str, len, "Lc inconsistent with TLV structure"); return;
	case 0x6A86: snprintf(str, len, "Incorrect P1 or P2 parameter"); return;
	case 0x6A87: snprintf(str, len, "Lc inconsistent with P1-P2"); return;
	case 0x6A88: snprintf(str, len, "Referenced data not found"); return;
	case 0x6A89: snprintf(str, len, "File already exists"); return;
	case 0x6A8A: snprintf(str, len, "DF name already exists"); return;
	case 0x6AF0: snprintf(str, len, "Wrong parameter value"); return;
	case 0x6B00: snprintf(str, len, "Wrong parameter(s) P1-P2"); return;
	case 0x6C00: snprintf(str, len, "Incorrect P3 length"); return;
	case 0x6D00: snprintf(str, len, "Instruction code not supported or invalid"); return;
	case 0x6E00: snprintf(str, len, "Class not supported"); return;
	case 0x6F00: snprintf(str, len, "Command aborted – more exact diagnosis not possible (e.g., operating system error)"); return;
	case 0x6FFF: snprintf(str, len, "Card dead (overuse, …)"); return;
	case 0x9000: snprintf(str, len, "I Command successfully executed (OK)"); return;
	case 0x9004: snprintf(str, len, "PIN not succesfully verified, 3 or more PIN tries left"); return;
	case 0x9008: snprintf(str, len, "Key/file not found"); return;
	case 0x9080: snprintf(str, len, "Unblock Try Counter has reached zero"); return;
	case 0x9100: snprintf(str, len, "OK"); return;
	case 0x9101: snprintf(str, len, "States.activity, States.lock Status or States.lockable has wrong value"); return;
	case 0x9102: snprintf(str, len, "Transaction number reached its limit"); return;
	case 0x910C: snprintf(str, len, "No changes"); return;
	case 0x910E: snprintf(str, len, "Insufficient NV-Memory to complete command"); return;
	case 0x911C: snprintf(str, len, "Command code not supported"); return;
	case 0x911E: snprintf(str, len, "CRC or MAC does not match data"); return;
	case 0x9140: snprintf(str, len, "Invalid key number specified"); return;
	case 0x917E: snprintf(str, len, "Length of command string invalid"); return;
	case 0x919D: snprintf(str, len, "Not allow the requested command"); return;
	case 0x919E: snprintf(str, len, "Value of the parameter invalid"); return;
	case 0x91A0: snprintf(str, len, "Requested AID not present on PICC"); return;
	case 0x91A1: snprintf(str, len, "Unrecoverable error within application"); return;
	case 0x91AE: snprintf(str, len, "Authentication status does not allow the requested command"); return;
	case 0x91AF: snprintf(str, len, "Additional data frame is expected to be sent"); return;
	case 0x91BE: snprintf(str, len, "Out of boundary"); return;
	case 0x91C1: snprintf(str, len, "Unrecoverable error within PICC"); return;
	case 0x91CA: snprintf(str, len, "Previous Command was not fully completed"); return;
	case 0x91CD: snprintf(str, len, "PICC was disabled by an unrecoverable error"); return;
	case 0x91CE: snprintf(str, len, "Number of Applications limited to 28"); return;
	case 0x91DE: snprintf(str, len, "File or application already exists"); return;
	case 0x91EE: snprintf(str, len, "Could not complete NV-write operation due to loss of power"); return;
	case 0x91F0: snprintf(str, len, "Specified file number does not exist"); return;
	case 0x91F1: snprintf(str, len, "Unrecoverable error within file"); return;
	case 0x9210: snprintf(str, len, "Insufficient memory. No more storage available"); return;
	case 0x9240: snprintf(str, len, "Writing to EEPROM not successful"); return;
	case 0x9301: snprintf(str, len, "Integrity error"); return;
	case 0x9302: snprintf(str, len, "Candidate S2 invalid"); return;
	case 0x9303: snprintf(str, len, "Application is permanently locked"); return;
	case 0x9400: snprintf(str, len, "No EF selected"); return;
	case 0x9401: snprintf(str, len, "Candidate currency code does not match purse currency"); return;
	case 0x9402: snprintf(str, len, "Address range exceeded / Candidate amount too high"); return;
	case 0x9403: snprintf(str, len, "Candidate amount too low"); return;
	case 0x9404: snprintf(str, len, "FID not found, record not found or comparison pattern not found"); return;
	case 0x9405: snprintf(str, len, "Problems in the data field"); return;
	case 0x9406: snprintf(str, len, "Required MAC unavailable"); return;
	case 0x9407: snprintf(str, len, "Bad currency : purse engine has no slot with R3bc currency"); return;
	case 0x9408: snprintf(str, len, "Selected file type does not match command / R3bc currency not supported in purse engine"); return;
	case 0x9580: snprintf(str, len, "Bad sequence"); return;
	case 0x9681: snprintf(str, len, "Slave not found"); return;
	case 0x9700: snprintf(str, len, "PIN blocked and Unblock Try Counter is 1 or 2"); return;
	case 0x9702: snprintf(str, len, "Main keys are blocked"); return;
	case 0x9704: snprintf(str, len, "PIN not succesfully verified, 3 or more PIN tries left"); return;
	case 0x9784: snprintf(str, len, "Base key"); return;
	case 0x9785: snprintf(str, len, "Limit exceeded – C-MAC key"); return;
	case 0x9786: snprintf(str, len, "SM error – Limit exceeded – R-MAC key"); return;
	case 0x9787: snprintf(str, len, "Limit exceeded – sequence counter"); return;
	case 0x9788: snprintf(str, len, "Limit exceeded – R-MAC length"); return;
	case 0x9789: snprintf(str, len, "Service not available"); return;
	case 0x9802: snprintf(str, len, "No PIN defined"); return;
	case 0x9804: snprintf(str, len, "Access conditions not satisfied, authentication failed"); return;
	case 0x9835: snprintf(str, len, "ASK RANDOM or GIVE RANDOM not executed"); return;
	case 0x9840: snprintf(str, len, "PIN verification not successful"); return;
	case 0x9850: snprintf(str, len, "INCREASE or DECREASE could not be executed because a limit has been reached"); return;
	case 0x9862: snprintf(str, len, "Authentication Error, application specific (incorrect MAC)"); return;
	case 0x9900: snprintf(str, len, "1 PIN try left"); return;
	case 0x9904: snprintf(str, len, "PIN not succesfully verified, 1 PIN try left"); return;
	case 0x9985: snprintf(str, len, "Wrong status – Cardholder lock"); return;
	case 0x9986: snprintf(str, len, "Missing privilege"); return;
	case 0x9987: snprintf(str, len, "PIN is not installed"); return;
	case 0x9988: snprintf(str, len, "Wrong status – R-MAC state"); return;
	case 0x9A00: snprintf(str, len, "2 PIN try left"); return;
	case 0x9A04: snprintf(str, len, "PIN not succesfully verified, 2 PIN try left"); return;
	case 0x9A71: snprintf(str, len, "Wrong parameter value – Double agent AID"); return;
	case 0x9A72: snprintf(str, len, "Wrong parameter value – Double agent Type"); return;
	case 0x9D05: snprintf(str, len, "Incorrect certificate type"); return;
	case 0x9D07: snprintf(str, len, "Incorrect session data size"); return;
	case 0x9D08: snprintf(str, len, "Incorrect DIR file record size"); return;
	case 0x9D09: snprintf(str, len, "Incorrect FCI record size"); return;
	case 0x9D0A: snprintf(str, len, "Incorrect code size"); return;
	case 0x9D10: snprintf(str, len, "Insufficient memory to load application"); return;
	case 0x9D11: snprintf(str, len, "Invalid AID"); return;
	case 0x9D12: snprintf(str, len, "Duplicate AID"); return;
	case 0x9D13: snprintf(str, len, "Application previously loaded"); return;
	case 0x9D14: snprintf(str, len, "Application history list full"); return;
	case 0x9D15: snprintf(str, len, "Application not open"); return;
	case 0x9D17: snprintf(str, len, "Invalid offset"); return;
	case 0x9D18: snprintf(str, len, "Application already loaded"); return;
	case 0x9D19: snprintf(str, len, "Invalid certificate"); return;
	case 0x9D1A: snprintf(str, len, "Invalid signature"); return;
	case 0x9D1B: snprintf(str, len, "Invalid KTU"); return;
	case 0x9D1D: snprintf(str, len, "MSM controls not set"); return;
	case 0x9D1E: snprintf(str, len, "Application signature does not exist"); return;
	case 0x9D1F: snprintf(str, len, "KTU does not exist"); return;
	case 0x9D20: snprintf(str, len, "Application not loaded"); return;
	case 0x9D21: snprintf(str, len, "Invalid Open command data length"); return;
	case 0x9D30: snprintf(str, len, "Check data parameter is incorrect (invalid start address)"); return;
	case 0x9D31: snprintf(str, len, "Check data parameter is incorrect (invalid length)"); return;
	case 0x9D32: snprintf(str, len, "Check data parameter is incorrect (illegal memory check area)"); return;
	case 0x9D40: snprintf(str, len, "Invalid MSM Controls ciphertext"); return;
	case 0x9D41: snprintf(str, len, "MSM controls already set"); return;
	case 0x9D42: snprintf(str, len, "Set MSM Controls data length less than 2 bytes"); return;
	case 0x9D43: snprintf(str, len, "Invalid MSM Controls data length"); return;
	case 0x9D44: snprintf(str, len, "Excess MSM Controls ciphertext"); return;
	case 0x9D45: snprintf(str, len, "Verification of MSM Controls data failed"); return;
	case 0x9D50: snprintf(str, len, "Invalid MCD Issuer production ID"); return;
	case 0x9D51: snprintf(str, len, "Invalid MCD Issuer ID"); return;
	case 0x9D52: snprintf(str, len, "Invalid set MSM controls data date"); return;
	case 0x9D53: snprintf(str, len, "Invalid MCD number"); return;
	case 0x9D54: snprintf(str, len, "Reserved field error"); return;
	case 0x9D55: snprintf(str, len, "Reserved field error"); return;
	case 0x9D56: snprintf(str, len, "Reserved field error"); return;
	case 0x9D57: snprintf(str, len, "Reserved field error"); return;
	case 0x9D60: snprintf(str, len, "MAC verification failed"); return;
	case 0x9D61: snprintf(str, len, "Maximum number of unblocks reached"); return;
	case 0x9D62: snprintf(str, len, "Card was not blocked"); return;
	case 0x9D63: snprintf(str, len, "Crypto functions not available"); return;
	case 0x9D64: snprintf(str, len, "No application loaded"); return;
	case 0x9E00: snprintf(str, len, "PIN not installed"); return;
	case 0x9E04: snprintf(str, len, "PIN not succesfully verified, PIN not installed"); return;
	case 0x9F00: snprintf(str, len, "PIN blocked and Unblock Try Counter is 3"); return;
	case 0x9F04: snprintf(str, len, "PIN not succesfully verified, PIN blocked and Unblock Try Counter is 3"); return;
	}

	if ((code & 0xff00) == 0x6100) {
		snprintf(str, len, "%u bytes available, use GET RESPONSE", code & 0x00ff);  // GET RESPONSE = APDU 00c00000nn
		return;
	}

	if ((code & 0xff00) == 0x9f00) {
		snprintf(str, len, "%u bytes available, use GET RESPONSE", code & 0x00ff);  // GET RESPONSE = APDU 00c00000nn
		return;
	}

	if ((code & 0xff00) == 0x6c00) {
		snprintf(str, len, "Bad length value in Le; '%02x' is the correct exact Le", code & 0x00ff);
		return;
	}

	snprintf(str, len, "Unknown error");
}
