#pragma once

void apdu_logapdu(uint8_t *apdu, size_t apdulen, int out);
void apdu_logstart();
void apdu_logstop();
void apdu_logmode(int mode);
