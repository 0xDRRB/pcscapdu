#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <err.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <PCSC/wintypes.h>
#include <PCSC/winscard.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <glib.h>

#include "color.h"
#include "pcscapdu.h"
#include "statusres.h"
#include "hist.h"
#include "log.h"
#include "util.h"
#include "pcsclua.h"

// Default conf values
int conf_histsize = HISTSIZE;
size_t conf_rapdumaxsz = RAPDUMAXSZ;
size_t conf_capdumaxsz = CAPDUMAXSZ;
int conf_color = COLOR;
int conf_log = LOG;
char *logfile;

SCARDCONTEXT hContext;
LPSTR mszReaders = NULL;
SCARDHANDLE hCard;
char **readers = NULL;
const SCARD_IO_REQUEST *pioSendPci;

int haveconfig;
GKeyFile *ini;

char **aliaskeys;
gsize nbraliases;
char **words;
char *commands[] = { "quit", "alias", "history", "help", "rawmode", "apdumode", NULL };
int wordsneedfree;

FILE *fplog;

int mode = APDUMODE;

uint8_t luataguid[10];
size_t luataguidlen;

lua_State *L;

char *
rl_commands_generator(const char *text, int state)
{
	static int command_index, len;
	char *command;

	if (!state) {
		command_index = 0;
		len = strlen(text);
	}

	// search in wordlist
	while ((command = words[command_index++])) {
		if (strncmp(command, text, len) == 0)
			return strdup(command);
	}

	return NULL;
}

char **
rl_commands_completion(const char *text, int start, int end)
{
	// our list of completions is final - no path completion
	rl_attempted_completion_over = 1;
	return rl_completion_matches(text, rl_commands_generator);
}

static void
sighandler(int sig)
{
	printf("Caught signal %d\n", sig);
	// FIXME cleanup !
	exit(EXIT_FAILURE);
}

// return 1 if we have config
int
apdu_initconfig()
{
	char *home;
	char *cfile;
	int confpathsz;
	GError *gerr = NULL;

	if ((home=getenv("HOME")) == NULL) {
		err(EXIT_FAILURE, "Unable to get $HOME");
	}

	confpathsz = strlen(home) + 1 + strlen(CONFFILE) + 1;
	if ((cfile = (char *) malloc(confpathsz)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error: ");
	}

	if (snprintf(cfile, confpathsz, "%s/%s", home, CONFFILE) != confpathsz-1) {
		warnx("Configuration file path error");
	}

	ini = g_key_file_new();
	if (g_key_file_load_from_file(ini, cfile, G_KEY_FILE_KEEP_COMMENTS, &gerr) == FALSE) {
		if (gerr->code != G_FILE_ERROR_NOENT)
			warnx("Error loading config file: %s (%d)", gerr->message, gerr->code);
		g_key_file_free(ini);
		ini = NULL;
		if (gerr != NULL) g_clear_error(&gerr);
		free(cfile);
		return(0);
	}

	free(cfile);

	return(1);
}

// Transmit ADPU from hex string
int
strcardtransmit(SCARDHANDLE hCard, const char *line, uint8_t *rapdu, size_t *rapdulen, int quiet)
{
	uint8_t *capdu = NULL;
	size_t capdulen = 0;

	// linelen >0 & even
	if (!strlen(line) || strlen(line) > conf_capdumaxsz*2)
		return(-1);

	if (!(capdu = malloc(strlen(line)/2))) {
		warn("malloc list error: ");
		cleanup();
		exit(EXIT_FAILURE);
	}

	if (!(capdulen = hex2array(line, capdu, strlen(line)/2))) {
		warnx("Invalid hex string!");
		if (capdu) free(capdu);
		return(-1);
	}

	if (cardtransmit(hCard, capdu, capdulen, rapdu, rapdulen, quiet) < 0) {
		if (capdu) free(capdu);
		return(-1);
	}

	if (capdu) free(capdu);
	return(0);
}

// Transmit ADPU from uint8_t array
int
cardtransmit(SCARDHANDLE hCard, uint8_t *capdu, size_t capdulen, uint8_t *rapdu, size_t *rapdulen, int quiet)
{
	LONG rv;
    DWORD res;
    size_t szPos;
	uint16_t status;
	char strstatus[128];

	res = conf_rapdumaxsz;

	if (!quiet) {
		printf("%s=> ", conf_color ? YELLOW : "" );
		for (szPos = 0; szPos < capdulen; szPos++) {
			printf("%02x ", capdu[szPos]);
		}
		printf("%s\n", conf_color ? RESET : "");
	}

	if ((rv = SCardTransmit(hCard, pioSendPci, capdu, capdulen, NULL, rapdu, &res)) != SCARD_S_SUCCESS) {
		warnx("SCardTransmit error: %s", pcsc_stringify_error(rv));
		*rapdulen = 0;
		return(-1);
	}

	// log transmit
	apdu_logapdu(capdu, capdulen, 1);

	if (mode == APDUMODE) {
		status = (rapdu[res - 2] << 8) | rapdu[res - 1];

		if (status == S_SUCCESS || status == S_OK || status == S_MORE) {
			if (!quiet)
				printf("%s<= ", conf_color ? GREEN : "");
		} else {
			if (!quiet)
				printf("%s<= ", conf_color ? RED : "");
		}
	} else {
		if (!quiet)
			printf("%s<= ", conf_color ? GREEN : "");
	}

	for (szPos = 0; szPos < res; szPos++) {
		if (mode == APDUMODE) {
			status = (rapdu[res - 2] << 8) | rapdu[res - 1];
			if (szPos >= res - 2) {
				if (status == S_SUCCESS || status == S_OK || status == S_MORE) {
					if (!quiet)
						printf(BOLDGREEN);
				} else {
					if (!quiet)
						printf(BOLDRED);
				}
			}
		}
		if (!quiet)
			printf("%02x ", rapdu[szPos]);
	}

	if (!quiet) {
		if (mode == APDUMODE)
			if (status == S_MORE)
				printf("  ()");
	}

	if (!quiet)
		printf("%s(%ld)\n", conf_color ? RESET : "", res);

	// log receive
	apdu_logapdu(rapdu, res, 0);

	if (mode == APDUMODE && !quiet) {
		if (status != S_SUCCESS && status != S_OK && status != S_MORE) {
			getstrstatus(status, strstatus, 128);
			if ((status & 0xff00) != 0x6100 && (status & 0xff00) != 0x9f00) {
				printf("Error: %s (0x%04x)\n", strstatus, status);
				return(-1);
			} else {
				printf("Warning: %s (0x%04x)\n", strstatus, status);
				return(0);
			}
		}
	}

	*rapdulen = (size_t)res;

	return(0);
}

void
failquit()
{
	cleanup();
	exit(EXIT_FAILURE);
}

void cleanup()
{
	LONG rv;

	if (hCard) {
		if ((rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD)) != SCARD_S_SUCCESS)
			warnx("SCardDisconnect error: %s", pcsc_stringify_error(rv));
	}

	if (mszReaders)
		SCardFreeMemory(hContext, mszReaders);

	if (hContext) {
		if ((rv = SCardReleaseContext(hContext)) != SCARD_S_SUCCESS)
			warnx("SCardReleaseContext error: %s", pcsc_stringify_error(rv));
	}

	if (readers)
		free(readers);

	if (conf_log && logfile && fplog) {
		apdu_logstop();
		fclose(fplog);
	}

	if (words && wordsneedfree) free(words);
	if (aliaskeys) g_strfreev(aliaskeys);
	if (ini) g_key_file_free(ini);
}

void
showaliases()
{
	int i = 0;
	char *val;
	GError *gerr = NULL;

	if (!nbraliases || !haveconfig) {
		printf("No alias\n");
		return;
	}

	printf("Defined aliases:\n");
	while (aliaskeys[i]) {
		val = g_key_file_get_value(ini, "aliases", aliaskeys[i], &gerr);
		if (gerr) {
			g_clear_error(&gerr);
		} else {
			printf("  %s = %s%s\n", aliaskeys[i], val, strlen(val) == 0 ? "<empty!>" : "");
			g_free(val);
		}
		i++;
	}
}

void
switchmode(int targetmode)
{
	mode = targetmode;
	apdu_logmode(mode);
}

int
main(int argc, char**argv)
{
	int i, j;
	char *endptr;
	char *in;
	char *fhistory;

	uint8_t *resp;
	size_t respsz;

	int retopt;
	int optlistdev = 0;
	char *optluascript = NULL;

	GError *gerr = NULL;
	char *aliasval;
	int nbr_commands = 0;

	char *tmplogfile = NULL;

	LONG rv;
	int nbReaders;
	DWORD dwReaders;
	unsigned int readernum = 0;
	char *ptr;
	DWORD dwActiveProtocol, dwReaderLen, dwState, dwProt, dwAtrLen;
	BYTE pbAtr[MAX_ATR_SIZE] = "";
	char pbReader[MAX_READERNAME] = "";

	while ((retopt = getopt(argc, argv, "hlrd:s:")) != -1) {
		switch (retopt) {
		case 'l':
			optlistdev = 1;
			break;
		case 'r':
			mode = RAWMODE;
			break;
		case 'd':
			readernum = (unsigned int)strtol(optarg, &endptr, 10);
			if (endptr == optarg)
				errx(EXIT_FAILURE, "Error: Invalid reader number");
			break;
		case 's':
			optluascript = strdup(optarg);
			break;
		case 'h':
			printhelp(argv[0]);
			return(EXIT_FAILURE);
		default:
			printhelp(argv[0]);
			return(EXIT_FAILURE);
		}
	}

	if (signal(SIGINT, &sighandler) == SIG_ERR) {
		printf("Error: Can't catch SIGINT\n");
		return(EXIT_FAILURE);
	}

	if (signal(SIGTERM, &sighandler) == SIG_ERR) {
		printf("Error: Can't catch SIGTERM\n");
		return(EXIT_FAILURE);
	}

	// Initialize pcsclite and set context
	if((rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext)) != SCARD_S_SUCCESS)
		errx(EXIT_FAILURE, "SCardEstablishContext: Cannot Connect to Resource Manager");

	/* Retrieve the available readers list. */
	dwReaders = SCARD_AUTOALLOCATE;
	if((rv = SCardListReaders(hContext, NULL, (LPSTR)&mszReaders, &dwReaders)) != SCARD_S_SUCCESS)
		errx(EXIT_FAILURE, "SCardListReaders error: %s", pcsc_stringify_error(rv));

	/* Extract readers from the null separated string and get the total number of readers */
	nbReaders = 0;
	ptr = mszReaders;
	while (*ptr != '\0') {
		ptr += strlen(ptr)+1;
		nbReaders++;
	}

	if (nbReaders == 0) {
		warnx("No reader found");
		failquit();
	}

	/* allocate the readers table */
	readers = calloc(nbReaders, sizeof(char *));
	if (readers == NULL) {
		warnx("Not enough memory for readers[]");
		failquit();
	}
	printf("%d reader(s) found\n", nbReaders);

	/* fill the readers table */
	nbReaders = 0;
	ptr = mszReaders;
	while (*ptr != '\0') {
		if(optlistdev)
			printf("%d: %s\n", nbReaders, ptr);
		readers[nbReaders] = ptr;
		ptr += strlen(ptr)+1;
		nbReaders++;
	}

	if (optlistdev) {
		cleanup();
		return(EXIT_SUCCESS);
	}

	printf(PCSCAPDUVERSION);

	// load configuration
	haveconfig = apdu_initconfig();

	// get command history from config file (or use default)
	if (haveconfig) {
		conf_histsize = g_key_file_get_integer(ini, "general", "histsize", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'histsize' in configuration file. Using default.");
			conf_histsize = HISTSIZE;
			g_clear_error(&gerr);
		}
	}

	// get color enable from config file (or use default)
	if (haveconfig) {
		conf_color = g_key_file_get_boolean(ini, "general", "color", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid boolean for 'color' in configuration file. Using default.");
			conf_color = COLOR;
			g_clear_error(&gerr);
		}
	}

	// get log enable from config file
	if (haveconfig) {
		conf_log = g_key_file_get_boolean(ini, "general", "enable_log", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid boolean for 'enable_log' in configuration file. Using default.");
			// use default
			conf_log = LOG;
			g_clear_error(&gerr);
		}
		if (conf_log) {
			tmplogfile = g_key_file_get_string(ini, "general", "logfile", &gerr);

			if (gerr) {
				if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
					warnx("Invalid string for 'logfile' in configuration file. Using default.");
				// use default
				logfile = strdup(LOGFILE);
				g_clear_error(&gerr);
			} else {
				if (!strlen(tmplogfile)) {
					warnx("Error: Empty logfile name!");
					logfile = strdup(LOGFILE);
				} else {
					logfile = strdup(tmplogfile);
				}
				g_free(tmplogfile);
			}
		}
	}

	// get max size of response APDU buffer from config file (or use default)
	if (haveconfig) {
		conf_rapdumaxsz = g_key_file_get_integer(ini, "general", "rapdumaxsz", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'rapdumaxsz' in configuration file. Using default.");
			conf_rapdumaxsz = RAPDUMAXSZ;
			g_clear_error(&gerr);
		}
	}

	// get max size of command APDU buffer from config file (or use default)
	if (haveconfig) {
		conf_capdumaxsz = g_key_file_get_integer(ini, "general", "capdumaxsz", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'capdumaxsz' in configuration file. Using default.");
			conf_capdumaxsz = CAPDUMAXSZ;
			g_clear_error(&gerr);
		}
	}

	/* connect to a card */
	dwActiveProtocol = -1;
	if((rv = SCardConnect(hContext, readers[readernum], SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard, &dwActiveProtocol)) != SCARD_S_SUCCESS) {
		warnx("SCardConnect error: %s", pcsc_stringify_error(rv));
		failquit();
	}

	/* get card status */
	dwAtrLen = sizeof(pbAtr);
	dwReaderLen = sizeof(pbReader);
	if ((rv = SCardStatus(hCard, pbReader, &dwReaderLen, &dwState, &dwProt, pbAtr, &dwAtrLen)) != SCARD_S_SUCCESS) {
		warnx("SCardStatus error: %s", pcsc_stringify_error(rv));
		failquit();
	}

	printf("Reader: %s (length %ld bytes)\n", pbReader, dwReaderLen);
	switch(dwActiveProtocol) {
	case SCARD_PROTOCOL_T0:
		pioSendPci = SCARD_PCI_T0;
		printf("  Protocol: T0 (%ld)\n", dwActiveProtocol);
		break;
	case SCARD_PROTOCOL_T1:
		printf("  Protocol: T1 (%ld)\n", dwActiveProtocol);
		pioSendPci = SCARD_PCI_T1;
		break;
	default:
		warnx("Unknown protocol. Stop here.");
		failquit();
	}
	printf("  State:    0x%lX\n", dwState);
	printf("  Prot:     %ld\n", dwProt);
	printf("  ATR (%ld):", dwAtrLen);
	for (i = 0; i < dwAtrLen; i++)
		printf(" %02X", pbAtr[i]);
	printf("\n");

	if (conf_log && logfile) {
		fplog = fopen(logfile, "a");
		if (fplog == NULL) {
			warn("Error opening logfile: ");
			warnx("Logging disabled!");
			conf_log = 0;
		} else {
			printf("Logging enabled (%s openned)\n", logfile);
			apdu_logstart();
		}
	}

	// Load and run Lua script
	if(optluascript) {
		L = luaL_newstate();

		if (!L) {
			warnx("Lua state creation failed!");
			failquit();
		}

		luaL_openlibs(L);
		lua_register(L, "sendstrapdu", lua_sendstrapdu);
		lua_register(L, "sendapdu", lua_sendapdu);

		printf("\n");
		if (luaL_dofile(L, optluascript) != LUA_OK) {
			puts(lua_tostring(L, lua_gettop(L)));
			lua_pop(L, lua_gettop(L));
			lua_close(L);
			if(optluascript)
				free(optluascript);
			failquit();
		}

		lua_close(L);
		free(optluascript);

		cleanup();
		return(EXIT_SUCCESS);
	}

	// show aliases & load readline completion
	if (haveconfig) {
		aliaskeys = g_key_file_get_keys(ini, "aliases", &nbraliases, &gerr);
		if (gerr) {
			warnx("%s", gerr->message);
			words = commands;
			g_clear_error(&gerr);
		} else {
			printf("%lu alias%s loaded\n", nbraliases, nbraliases>1 ? "" : "");
			// merge commands to aliases to completion wordslist
			while (commands[nbr_commands]) nbr_commands++;
			if ((words = malloc(sizeof(char *) * (nbr_commands+nbraliases + 1))) == NULL) {
				warn("Words malloc Error: ");
				failquit();
			}
			i = 0;
			while (commands[i]) {
				words[i] = commands[i];
				i++;
			}
			j = 0;
			while (aliaskeys[j]) {
				words[i] = aliaskeys[j];
				i++; j++;
			}
			words[i] = 0;
			wordsneedfree = 1; // enable free(words) when we quit
		}
	} else {
		words = commands;
	}

	printf("Use 'help' to get a list of available commands.\n");

	// Load commands history
	apdu_inithistory(&fhistory);

	// Enable completion
	rl_attempted_completion_function = rl_commands_completion;

	// allocate R-APDU buffer
	if ((resp = malloc(sizeof(uint8_t) * conf_rapdumaxsz)) == NULL) {
		warn("resp[] malloc Error: ");
		failquit();
	}
	respsz = conf_rapdumaxsz;

	while ((in = readline(mode == APDUMODE ? "APDU> " : "RAW> ")) != NULL) {
		if (strlen(in) && !isblankline(in)) {
			// strip whitespace
			g_strstrip(in);
			// add to commands history
			apdu_addhistory(in);
			if (strcmp(in, "alias") == 0) {
				showaliases();
				free(in);
				continue;
			}
			if (strcmp(in, "history") == 0) {
				apdu_disphist();
				free(in);
				continue;
			}
			if (strcmp(in, "help") == 0) {
				apdu_help();
				free(in);
				continue;
			}
			if (strcmp(in, "rawmode") == 0) {
				switchmode(RAWMODE);
				free(in);
				continue;
			}
			if (strcmp(in, "apdumode") == 0) {
				switchmode(APDUMODE);
				free(in);
				continue;
			}
			if (strcmp(in, "quit") == 0) {
				free(in);
				break;
			}
			if (!nbraliases) {
				// no alias in config file
				if (strcardtransmit(hCard, in, resp, &respsz, 0) < 0) {
					warnx("cardtransmit error!");
				}
			} else {
				// look for alias
				aliasval = g_key_file_get_value(ini, "aliases", in, &gerr);
				if (gerr) {
					// alias not found, send what i have
					if (strcardtransmit(hCard, in, resp, &respsz, 0) < 0) {
						warnx("cardtransmit error!");
					}
					g_clear_error(&gerr);
				} else if (!strlen(aliasval)) {
					// alias resolv to ""
					printf("This alias resolv to an empty string. Fix your "CONFFILE" please.\n");
				} else {
					// alias found, use that
					if (strcardtransmit(hCard, aliasval, resp, &respsz, 0) < 0) {
						warnx("cardtransmit error!");
					}
				}
				g_free(aliasval);
			}
		}
		if (in)
			free(in);
	}
	printf("\n");

	apdu_closehistory(fhistory);

	if (optluascript)
		free(optluascript);

	if (resp)
		free(resp);

	cleanup();

	printf("Bye. Have a nice day\n");

	return(EXIT_SUCCESS);
}
